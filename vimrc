set encoding=utf-8
set nocompatible              " be iMproved, required
filetype off                  " required

" VIMPLUG
call plug#begin('~/.vim/plugged')

" UI AND VISUAL PLUGINS:
" ---------------------
Plug 'mhinz/vim-startify'                 " fancy start screen
    let g:startify_bookmarks = [
    \ {'v': '~/.vimrc'},
    \ {'i3': '~/.config/i3/config'},
    \ {'p': '~/.config/polybar/config'},
    \ {'c': '~/.config/nvim/coc-settings.json'}]
Plug 'junegunn/goyo.vim'                  " vim zen mode
    let g:goyo_height = 100
    nmap <Leader>z :Goyo<CR>
Plug 'vim-airline/vim-airline'            " status bar
    let g:airline#extensions#tabline#enabled = 1
Plug 'vim-airline/vim-airline-themes'     " themes for statusbar
Plug 'simnalamburt/vim-mundo'             " visual undo tree
    let g:mundo_right = 1
    nnoremap <F5> :MundoToggle<CR>
Plug 'ErichDonGubler/vim-sublime-monokai' " colorscheme
Plug 'sickill/vim-monokai'                " colorscheme
Plug 'tanvirtin/monokai.nvim'             " colorscheme
Plug 'srcery-colors/srcery-vim'           " colorscheme
Plug 'arcticicestudio/nord-vim'           " colorscheme
Plug 'dracula/vim', {'as': 'dracula'}     " colorscheme
Plug 'jeetsukumaran/vim-buffergator'      " buffer preview list
"Plug 'liuchengxu/vim-which-key'
"    nnoremap <silent> <leader> :WhichKey '<Space>'<CR>


" SYNTAX HIGHLIGHTING
" -------------------
Plug 'sheerun/vim-polyglot'
Plug 'HiPhish/guile.vim'
Plug 'lepture/vim-jinja'
Plug 'hylang/vim-hy'
Plug 'bakpakin/janet.vim'
Plug 'PotatoesMaster/i3-vim-syntax'
"Plug 'pangloss/vim-javascript'
"Plug 'zah/nim.vim'
"Plug 'posva/vim-vue'
"Plug 'mxw/vim-jsx'
"    let g:ycm_global_ycm_extra_conf = "~/.vim/bundle/.ycm_extra_conf.py"
"Plug 'andys8/vim-elm-syntax'
"Plug 'reasonml-editor/vim-reason-plus'
"Plug 'evanleck/vim-svelte'
"Plug 'elixir-editors/vim-elixir'
"Plug 'cespare/vim-toml'
"Plug 'peterhoeg/vim-qml'
"Plug 'bakpakin/fennel.vim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" PROGRAMMING PLUGINS:
" -------------------
"Plug 'ervandew/supertab'
"    let g:SuperTabDefaultCompletionType = '<c-n>'
"Plug 'w0rp/ale'               " async linter
"    let g:ale_enabled = 0
"    nnoremap <F3> :ALEToggle<CR>
Plug 'tpope/vim-surround'     " surround with...
"Plug 'ternjs/tern_for_vim'    " javascript analysis engine
Plug 'tpope/vim-fugitive'     " git
"Plug 'Valloric/YouCompleteMe', {'do': 'python install.py'} " code completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
    let g:coc_node_path = '/usr/bin/node'
    highlight CocHintFloat ctermfg=Red guifg=#ff0000
Plug 'jiangmiao/auto-pairs'
Plug 'eraserhd/parinfer-rust', {'do': 'cargo build --release'}
    nmap <Leader>P :ParinferOn<CR>
    nmap <Leader>PP :ParinferOff<CR>
    "nmap <Leader>p :ParinferOff<CR>
Plug 'psf/black', { 'branch': 'stable' }
autocmd BufWritePre *.py execute ':Black'


" disable plugins not running in GVim:
if has("gui_running")
else 
    Plug 'Olical/conjure'
endif

" SEARCH PLUGINS:
" --------------
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
    nnoremap <leader>ff <cmd>Telescope find_files<cr>
    nnoremap <leader>fg <cmd>Telescope live_grep<cr>
    nnoremap <leader>fb <cmd>Telescope buffers<cr>
    nnoremap <leader>fw <cmd>Telescope current_buffer_fuzzy_find<cr>
    nnoremap <leader>fh <cmd>Telescope help_tags<cr>
    nnoremap <leader>fr <cmd>Telescope resume<cr>
    nnoremap <leader>gs <cmd>Telescope git_status<cr>
    nnoremap <leader>gc <cmd>Telescope git_commits<cr>


Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
    nmap <Leader>rg :Rg<CR>
    nmap <Leader>ag :Ag<CR>
    "nmap <Leader>ff :Files<CR>
    nmap <Leader>bb :Buffers<CR>

Plug 'scrooloose/nerdtree'    " file tree
    nnoremap <F9> :NERDTreeToggle<CR>
    let g:NERDTreeHijackNetrw = 1
    let g:NERDTreeQuitOnOpen = 1
    "let g:NERDTreeMapOpenInTab = '<ENTER>'
Plug 'osyo-manga/vim-anzu'    " better search nmap
    nmap n <Plug>(anzu-n-with-echo)
    nmap N <Plug>(anzu-N-with-echo)
    nmap * <Plug>(anzu-star-with-echo)
    nmap # <Plug>(anzu-sharp-with-echo)
    set statusline=%{anzu#search_status()}

" OTHER PLUGINS:
" -------------
call plug#end()            " required by vimplug


" To ignore plugin indent changes, instead use:
filetype plugin indent on    " required



" MAIN CONFIG:
" -----------
" set leader key:
nnoremap <SPACE> <Nop>
map <Space> <leader>

" write / save file:
nmap <Leader>w :w<CR>

" window / split navigation:
nmap <Leader>\| :vsp<CR>
nmap <Leader>_ :sp<CR>

" switch vertical/horozontal split:
nmap <Leader>h <C-w>t<C-w>H
nmap <Leader>v <C-w>t<C-w>K

" realign split:
nmap <Leader>= <C-W>=

" move focus:
nmap <Leader><Left> <C-W><Left>
nmap <Leader><Right> <C-W><Right>
nmap <Leader><Up> <C-W><Up>
nmap <Leader><Down> <C-W><Down>

" buffers:
"nmap <Leader><b><n> :bn<CR>
"nmap <Leader><b><p> :bp<CR>
nmap <Leader>q :q<CR>
nmap <Leader>bd :bd<CR>

" replace word under cursor:
nmap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>

" run hy code:
"nmap <Leader>hy :!hy %<CR>
nmap <Leader>hy :w<CR>:!hy %<CR>
nmap <Leader>py :w<CR>:!python %<CR>
nmap <Leader>pdb :w<CR>:term python -m ipdb %<CR>


" REMAPS
" ------
" yank from cursor to end of line:
noremap Y y$
" press return to turn off highlights afeter search:
nnoremap <CR> :nohlsearch<CR><CR>
" undo breakpoints:
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" GVIM
" ----
set guifont=Inconsolata\ Regular\ 10
set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar



" STANDARD CONFIGURATION
" ----------------------
set background=dark
syntax on
" colorscheme torte
" colorscheme herald
"colorscheme sublimemonokai
"colorscheme molokai
colorscheme monokai
set mouse=a
set mousehide
set hlsearch
set incsearch
set number
set showcmd
set scrolloff=8
set wildmenu
set wildmode=list:longest,full
set backupdir=~/.vim/backupdir
set undodir=~/.vim/undodir
set undofile
set splitbelow splitright
set clipboard=unnamedplus 

" python:
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

" python-mode:
autocmd FileType python set colorcolumn=0
set completeopt=menu
set nofoldenable

function! RemoveTrailingSpaces()
        :%s/\s\+$//e
endfunction

" :w!! write the file when you accidentally opened it without the right (root) privileges
cmap w!! w !sudo tee % > /dev/null
