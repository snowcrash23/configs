require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  
  -- LSP Configuration & Plugins
  use { 
    'neovim/nvim-lspconfig',
    requires = {
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
      'j-hui/fidget.nvim',-- Useful status updates for LSP
      'folke/neodev.nvim', -- Additional lua configuration, makes nvim stuff amazing
    },
  }
  
  -- Autocompletion
  use {
    'hrsh7th/nvim-cmp',
    requires = {'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-nvim-lua',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip'},
  }
  use "rafamadriz/friendly-snippets"
  
  -- Highlight, edit, and navigate code
  use { 'nvim-treesitter/nvim-treesitter',
    run = function() pcall(require('nvim-treesitter.install').update { with_sync = true }) end }
  
  -- Additional text objects via treesitter
  use { 'nvim-treesitter/nvim-treesitter-textobjects', after = 'nvim-treesitter' }

  -- Git
  use 'tpope/vim-fugitive'
  
  -- Themes
  use 'navarasu/onedark.nvim'
  use 'tanvirtin/monokai.nvim'
  
  -- Other plugins
  use 'numToStr/Comment.nvim' -- "gc" to comment visual regions/lines
  use 'tpope/vim-sleuth' -- Detect tabstop and shiftwidth automatically
  use 'tpope/vim-eunuch' -- Move, Rename, Delete, SudoWrite, SudoRead etc.
  use 'tpope/vim-surround'
  use 'mbbill/undotree'
  use { 'windwp/nvim-autopairs', config = function() require('nvim-autopairs').setup {} end }
  use 'ggandor/leap.nvim'

  -- Fuzzy Finder (files, lsp, etc)
  use { 'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { 'nvim-lua/plenary.nvim' } }
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable 'make' == 1 }

  -- LISP plugins
  --use 'gpanders/nvim-parinfer'


  -- Whichkey Plugin
  use {
    'folke/which-key.nvim',
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require('which-key').setup {
        -- your configuration comes here
      }
    end
  }
  -- LaTeX
  use 'lervag/vimtex'

  -- Zen Mode
  use({ "Pocco81/true-zen.nvim", config = function() require("true-zen").setup { } end })

end) 
