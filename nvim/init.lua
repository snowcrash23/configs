-- [[ Package manager ./lua/plugins.lua ]]
require("plugins")

-- [[ Setting options ./lua/options.lua ]]
require("options")

-- [[ Set Theme ]]
vim.cmd [[colorscheme monokai_pro]]

-- [[ Basic Keymaps / Remaps ]]
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for write, quit and bufer close
--vim.keymap.set('n', '<leader>q', '<cmd>q<cr>', { desc = 'Quit Neovim'})
vim.keymap.set('n', '<leader>w', '<cmd>w<cr>', { desc = 'Write Buffer'})
vim.keymap.set('n', '<leader>q', '<cmd>bd<cr>', { desc = 'Buffer Delete'})

-- Remap for running buffer through interpreter
vim.keymap.set('n', '<leader>py', '<cmd>!python %<cr>')

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Highlight on yank
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})


-- [[ Require Plugins]]
-- --------------------
require('leap').add_default_mappings()

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'molokai',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
  },
}

require('bufferline').setup{}

require('Comment').setup()

require('indent_blankline').setup {
  char = '┊',
  show_trailing_blankline_indent = false,
}

require('gitsigns').setup {
  signs = {
    add = { text = '+' },
    change = { text = '~' },
    delete = { text = '_' },
    topdelete = { text = '‾' },
    changedelete = { text = '~' },
  },
}
vim.keymap.set('n', '<leader>gs', vim.cmd.Git, { desc = '[G]it [S]tatus' })
vim.keymap.set('n', '<leader>gv', vim.cmd.Gvdifsplit, { desc = '[G]it [V]ertical diff' })

vim.keymap.set('n', '<leader>u',  vim.cmd.UndotreeToggle, { desc = 'Toggle Undotree' })



-- [[ Configure Telescope ]]
-- -------------------------
require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}
pcall(require('telescope').load_extension, 'fzf') -- Enable telescope fzf native, if installed
vim.keymap.set('n', '<leader>fr', require('telescope.builtin').oldfiles, { desc = '[F]ind  [R]ecently opened files' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>/', function()
  require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = 10,
    previewer = false,
  })
end, { desc = '[/] Fuzzily search in current buffer]' })
vim.keymap.set('n', '<leader>ff', require('telescope.builtin').find_files, { desc = '[F]ind [F]iles' })
vim.keymap.set('n', '<leader>fh', require('telescope.builtin').help_tags, { desc = '[F]ind [H]elp' })
vim.keymap.set('n', '<leader>fw', require('telescope.builtin').grep_string, { desc = '[F]ind [W]ord under cursor' })
vim.keymap.set('n', '<leader>fg', require('telescope.builtin').live_grep, { desc = '[F]ind by [G]rep' })
vim.keymap.set('n', '<leader>fd', require('telescope.builtin').diagnostics, { desc = '[F]ind [D]iagnostics' })


-- [[ Configure Treesitter ]]
require('nvim-treesitter.configs').setup {
  -- Add languages to be installed here that you want installed for treesitter
  ensure_installed = { 'c', 'cpp', 'go', 'lua', 'python', 'rust', 'typescript', 'help', 'vim' },

  highlight = { enable = true },
  indent = { enable = true, disable = { 'python' } },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      scope_incremental = '<c-s>',
      node_decremental = '<c-backspace>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
    },
  },
}

-- Diagnostic features
vim.diagnostic.config {
  underline = false,
  virtual_text = false,
  signs = true
}

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>E', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>Q', vim.diagnostic.setloclist)
--[[ vim.keymap.set("n", "<leader>Q", "<cmd>TroubleToggle<cr>",
  {silent = true, noremap = true}
) ]]



-- [[ LSP settings ]]
--------------------
-- [[ Setting options ./lua/lsp-settings.lua ]]
require("lsp-settings")

-- Turn on Lightbulb if code actions are available
require('nvim-lightbulb').setup({autocmd = {enabled = true}})


-- [ nvim-cmp setup ]
-- [[ Setting options ./lua/completion-setings.lua
require('completion-setings')

require('auto-session').setup()

-- [ Zenmode ]
-- vim.api.nvim_set_keymap("n", "<leader>zn", ":TZNarrow<CR>", {})
-- vim.api.nvim_set_keymap("v", "<leader>zn", ":'<,'>TZNarrow<CR>", {})
-- vim.api.nvim_set_keymap("n", "<leader>zf", ":TZFocus<CR>", {})
-- vim.api.nvim_set_keymap("n", "<leader>zm", ":TZMinimalist<CR>", {})
vim.api.nvim_set_keymap("n", "<leader>z", ":TZAtaraxis<CR>", {})

--[ Folding ]
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.foldingRange = {
  dynamicRegistration = false,
  lineFoldingOnly = true
}
local language_servers = require("lspconfig").util.available_servers() -- or list servers manually like {'gopls', 'clangd'}
for _, ls in ipairs(language_servers) do
  require('lspconfig')[ls].setup({
    capabilities = capabilities
    -- you can add other fields for setting up lsp server in this table
  })
end
require('ufo').setup()
vim.keymap.set('n', 'zR', require('ufo').openAllFolds)
vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)

--[ Filemanager Netrw ] 
vim.g['netrw_keepdir'] = 0
vim.g['netrw_winsize'] = 30
vim.g['netrw_banner'] = 0
vim.g['netrw_liststyle'] = 3
vim.g['netrw_localcopydircmd'] = 'cp -r'
--vim.g['netrw_altv'] = 1
vim.keymap.set('n', '<leader>L', '<cmd>Lexplore<cr>')

-- [ Terminal ]
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>') -- make Esc quit to normal mode.
vim.keymap.set('n', '<leader>tv', '<cmd>vsplit term://zsh<cr>', { desc = '[T]erminal [V]ertical'})
vim.keymap.set('n', '<leader>th', '<cmd>split term://zsh<cr>',  { desc = '[T]erminal [H]orizontal' })
vim.keymap.set('n', '<leader>w', '<cmd>w<cr>', { desc = 'Write Buffer'})


-- [ Other Plugin Configurations]
-- ------------------------------
--[ Startify ]
vim.g['startify_custom_header'] = ''
vim.g['startify_bookmarks'] = {'~/.config/nvim/init.lua', '~/.config/i3/config', '~/.config/polybar/config'}

-- LaTex
vim.g['vimtex_view_general_viewer'] = 'okular'
--vim.g['vimtex_view_general_options'] = '--unique file:@pdf\#src:@line@tex'

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
