# If you come from bash you might have to change your $PATH.
export PATH="$PATH:$HOME/bin"

# Path to your oh-my-zsh installation.
ZSH=/usr/share/oh-my-zsh/

#ZSH_THEME="agnoster"
#ZSH_THEME="avit"
#ZSH_THEME="af-magic"
ZSH_THEME="gnzh"

# CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
export UPDATE_ZSH_DAYS=14

# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"
# COMPLETION_WAITING_DOTS="true"
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# HIST_STAMPS="mm/dd/yyyy"
# ZSH_CUSTOM=/path/to/new-custom-folder
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
#plugins=(sudo fzf ripgrep httpie lein npm z poetry pyenv)
plugins=(fzf ripgrep httpie lein npm poetry)

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

export EDITOR='nvim'
export VISUAL='nvim'

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

ZSH_CACHE_DIR=$HOME/.oh-my-zsh-cache
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi
source $ZSH/oh-my-zsh.sh

# remove some aliases set by oh-my-zsh:
unalias 1 2 3 4 5 6 7 8 9 md rd - _

# my shell aliases:
# #
alias vi='vim'
alias vir='vim -R'
alias duh='du -h --max-depth=1 | sort -h'
alias delete_empty_dirs='find . -type d -empty -delete'
alias pipenv shell='pipenv shell -c'
alias ls='ls --color --group-directories-first'
alias l1='ls -1'
alias l='ls'

alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# pacman:
alias pacown='pacman -Qo'

# command replacements:
#alias cat='bat --theme="Monokai Extended"'
alias ncdu='ncdu --color dark -rr'
alias vim='nvim'
alias hy='hy --repl-output-fn=hy.contrib.hy-repr.hy-repr' # only for hy 0.20
alias ihy='jupyter console --kernel calysto_hy'
alias tidy='tidy -q --tidy-mark no -i'
alias ytm3p='yt-dlp -x --audio-format mp3'
alias clj-repl-rebel='clojure -M:repl/rebel'
alias bb='rlwrap bb'
alias sqlite-cli='litecli'
#alias pyinfra='python -m pyinfra'

# BAT
alias bat='bat --style=numbers --color=always --theme="Monokai Extended"'
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"

# FZF
export FZF_DEFAULT_OPTS="--preview \
        '([[ -f {} ]] && (bat {} || cat {})) \
        || ([[ -d {} ]] && (tree -C {} | less)) \
        || echo {} 2> /dev/null | head -200'
        --bind '?:toggle-preview'
        --bind 'ctrl-a:select-all'
        --bind 'ctrl-y:execute-silent(echo -n {2:..} | pbcopy)+abort'
        --bind 'ctrl-e:execute(nvim {+})'"
#        --bind 'ctrl-v:execute(nvim {})+abort'"

function finddirs() { fd -t d | fzf --multi}
zle -N finddirs
bindkey '^v' finddirs

function findfiles() { fd -t f | fzf --multi}
zle -N findfiles
bindkey '^f' findfiles

function findpictures() { fd -e png -e jpg | fzf --multi --preview (chafa {}) }
zle -N findpictures
bindkey '^k' findpictures


# disable ctrl+d close:
set -o ignoreeof

# prevent duplicate history entrys:
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt PUSHD_IGNORE_DUPS


# MY FUNCTIONS
function killproc-fzf {
    local pid=$(ps -ef | sed 1d | eval "fzf ${FZF_DEFAULT_OPTS} -m --header='[kill:process]'" | awk '{print $2}')
    if [ "x$pid" != "x" ]
    then
      echo $pid | xargs kill -${1:-9}
    fi 
}

# opam (ocaml package manager) configuration
test -r ~/.opam/opam-init/init.zsh && . ~/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# perl/CPAN
#PATH="/home/rs/perl5/bin${PATH:+:${PATH}}"; export PATH;
#PERL5LIB="/home/rs/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
#!/bin/bashPERL_LOCAL_LIB_ROOT="/home/rs/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
#PERL_MB_OPT="--install_base \"/home/rs/perl5\""; export PERL_MB_OPT;
#PERL_MM_OPT="INSTALL_BASE=/home/rs/perl5"; export PERL_MM_OPT;
