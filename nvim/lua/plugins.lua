require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  
  -- LSP Configuration & Plugins
  use { 
    'neovim/nvim-lspconfig',
    requires = {
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',
      'j-hui/fidget.nvim',-- Useful status updates for LSP
      'folke/neodev.nvim', -- Additional lua configuration, makes nvim stuff amazing
    },
  }
  
  -- Autocompletion
  use {
    'hrsh7th/nvim-cmp',
    requires = {'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'saadparwaiz1/cmp_luasnip',
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-nvim-lua',
      'L3MON4D3/LuaSnip',
      'saadparwaiz1/cmp_luasnip'},
  }
  use "rafamadriz/friendly-snippets"
  
  -- Highlight, edit, and navigate code
  use { 'nvim-treesitter/nvim-treesitter',
    run = function() pcall(require('nvim-treesitter.install').update { with_sync = true }) end }
  
  -- Additional text objects via treesitter
  use { 'nvim-treesitter/nvim-treesitter-textobjects', after = 'nvim-treesitter' }

  -- Folding
  use {'kevinhwang91/nvim-ufo', requires = 'kevinhwang91/promise-async'}
  
  -- Git
  use 'tpope/vim-fugitive'
  use 'lewis6991/gitsigns.nvim'
  
  -- Themes
  use 'navarasu/onedark.nvim'
  use 'tanvirtin/monokai.nvim'
  
  -- Other plugins
  use { 'nvim-lualine/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', opt = false }} -- Fancier statusline
  use 'lukas-reineke/indent-blankline.nvim' -- Add indentation guides even on blank lines
  use 'numToStr/Comment.nvim' -- "gc" to comment visual regions/lines
  use 'tpope/vim-sleuth' -- Detect tabstop and shiftwidth automatically
  use 'tpope/vim-eunuch' -- Move, Rename, Delete, SudoWrite, SudoRead etc.
  use 'tpope/vim-surround'
  use 'mhinz/vim-startify'
  use 'mbbill/undotree'
  use { 'akinsho/bufferline.nvim', tag = "v3.*", requires = 'nvim-tree/nvim-web-devicons'}
  use { 'windwp/nvim-autopairs', config = function() require('nvim-autopairs').setup {} end }
  use 'ggandor/leap.nvim'
  use 'kosayoda/nvim-lightbulb'
  use { 'weilbith/nvim-code-action-menu', cmd = 'CodeActionMenu' }

  -- Fuzzy Finder (files, lsp, etc)
  use { 'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { 'nvim-lua/plenary.nvim' } }
  use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', cond = vim.fn.executable 'make' == 1 }

  -- LISP plugins
  use 'Olical/conjure'
  use 'gpanders/nvim-parinfer'

  -- Trouble
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      require("trouble").setup {
        -- your configuration comes here
      }
    end
  }

  -- Session
  use {
    'rmagatti/auto-session',
    config = function()
      require("auto-session").setup {
        log_level = "error",
        auto_session_suppress_dirs = { "~/", "~/projects", "~/Downloads", "/"},
      }
    end
  }
  use {
    'rmagatti/session-lens',
    requires = {'rmagatti/auto-session', 'nvim-telescope/telescope.nvim'},
    config = function()
      require('session-lens').setup({--[[your custom config--]]})
    end
  }

  -- Whichkey Plugin
  use {
    'folke/which-key.nvim',
    config = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
      require('which-key').setup {
        -- your configuration comes here
      }
    end
  }
  -- LaTeX
  use 'lervag/vimtex'

  -- Zen Mode
  use({ "Pocco81/true-zen.nvim", config = function() require("true-zen").setup { } end })

  -- D2 diagram support
  use 'terrastruct/d2-vim'

  -- Add custom plugins to packer from ~/.config/nvim/lua/custom/plugins.lua
  local has_plugins, plugins = pcall(require, 'custom.plugins')
  if has_plugins then
    plugins(use)
  end
end) 
